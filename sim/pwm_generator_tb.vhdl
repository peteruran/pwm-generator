library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity pwm_generator_tb is
end entity;

architecture behave of pwm_generator_tb is

    signal clk          : std_logic;
    signal reset_n      : std_logic;

    constant clock_period : time := 20 ns;

begin

    i_pwm_generator : entity work.pwm_generator
    port map (
        clk         => clk,
        reset_n     => reset_n
    );

    reset_gen : process
    begin
        reset_n <= '0';
        wait for 5*clock_period;
        reset_n <= '1';
        wait;
    end process;

    clocking : process
    begin
        clk <= '0';
        wait for clock_period;
        clk <= '1';
        wait for clock_period;
    end process;

end architecture;
