library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity pwm_generator is

    generic (
        c_counter_size  : natural := 10
    );

    port (
        clk             : in std_logic;
        reset_n         : in std_logic;
        pwm_output      : out std_logic
    );

end entity;


architecture rtl of pwm_generator is

    signal counter      : unsigned (c_counter_size - 1 downto 0);
    signal limit      : unsigned (c_counter_size - 1 downto 0);
    signal duty_cycle   : unsigned (5 downto 0) := to_unsigned(50, 6);

begin

    process(clk)

    begin

        if reset_n = '0' then

            counter <= (others => '0');

        elsif rising_edge(clk) then

            counter <= counter + 1;
            limit <= ((2 ** c_counter_size) - 1) * duty_cycle;

            if (counter * duty_cycle > ((2 ** c_counter_size) - 1) * duty_cycle) then
                pwm_output <= '1';
            else
                pwm_output <= '0';
            end if;

        end if;

    end process;

end;
